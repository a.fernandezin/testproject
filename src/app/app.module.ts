import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyComponentComponent } from './my-component/my-component.component';
import { MydirecDirective } from './my-component/mydirec.directive';
import { Mycomp2Component } from './mycomp2/mycomp2.component';
import { InsideComponent } from './inside/inside.component';
import { GroupComponent } from './group/group.component';

@NgModule({
  declarations: [	
    AppComponent,
      MyComponentComponent,
      MydirecDirective,
      Mycomp2Component,
      InsideComponent,
      GroupComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
