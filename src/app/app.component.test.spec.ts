import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {render, screen, fireEvent} from '@testing-library/angular'
import { BrowserModule, By } from '@angular/platform-browser';
import { MydirecDirective } from './my-component/mydirec.directive';
import { Mycomp2Component } from './mycomp2/mycomp2.component';
import { InsideComponent } from './inside/inside.component';
import { GroupComponent } from './group/group.component';
import { MyComponentComponent } from './my-component/my-component.component';
import { AppCompModule } from './appcomp.module';


fdescribe('AppComponent', () => {
  beforeEach(async () => {
   
  });

  it('should create the component', async() => {
    const {fixture,detectChanges}=await render( AppComponent, {
      imports:[BrowserModule ,AppCompModule  ],
      declarations:[
     ],
      componentProperties: {},
    })

    const insides=fixture.debugElement.queryAll(By.directive(InsideComponent))
    console.log(insides)
    expect(insides.length).toBe(10)
    expect(insides[0].componentInstance instanceof InsideComponent).toBeTruthy() 


  });

  });
