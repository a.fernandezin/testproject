import { User } from "./my-component/models";
import { Store } from "./store";

export interface State {
  currentUser: User | null;
  data?:{
    dataa?:string;
  datab?:string;
  datac?:string;


  }
  }

export class UserStore extends Store<State> {
  constructor() {
    super({
      currentUser: null,
    });
  }

  get currentUser$() {
    return this.select(state => state.currentUser);
  }
  get currentUser() {
    return this.selectSync(state => state.currentUser);
  }

  get data$(){
    return this.select(({data}) => (data));
  }

  setCurrentUser(user: User) {
    this.dispatch(state => ({
      ...state,
      currentUser: user,
    }));
  }
}