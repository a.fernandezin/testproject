import { AfterContentInit, AfterViewInit, ChangeDetectorRef, Component, ContentChild, ContentChildren, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { BehaviorSubject, combineLatest, map, Observable } from 'rxjs';
import { Mycomp2Component } from '../mycomp2/mycomp2.component';
import { UserStore } from '../Mysstore';
import { Filter, insertUpdate, updateFilter, User, users, users2 } from './models';
import { MydirecDirective } from './mydirec.directive';
import { create } from "rxjs-spy";
import { InsideComponent } from '../inside/inside.component';
const spy = create();
@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.css'],
})
export class MyComponentComponent implements OnInit, AfterContentInit, AfterViewInit {
  @ContentChild(MydirecDirective)
  myDirec: MydirecDirective | undefined;
  myContext = { $implicit: 'World', local: 'Svet' };

  @ViewChild('ref', { static: false }) comp2!: Mycomp2Component;

  @ContentChildren(Mycomp2Component, { descendants: true }) comp2c: any;
  @ContentChild('mitemp', { }) mitemp: any;
  @ContentChild('mitemp2', { }) mitemp2: any;

  @ContentChildren(InsideComponent, { descendants: true,emitDistinctChangesOnly:false }) midiv!: QueryList<InsideComponent>;

  @ViewChildren('midiv', {emitDistinctChangesOnly:true  }) midiv2: any;

  showInside=false

  subjectFilters = new BehaviorSubject<Filter[]>([]);
  subjectUsers = new BehaviorSubject<User[]>([]);
  usersFiltered$!: Observable<User[]>;
  subjectMask = new BehaviorSubject<string[]>([]);

  store=  new UserStore();

  constructor(public ch:ChangeDetectorRef) {}
  ngAfterViewInit(): void {
    console.log('comp2', this.comp2, this.comp2c);
    console.log('midiv',this.mitemp, this.midiv);

    // this.midiv.forEach(m=>{
    //   m.data='OKKKDIVVVV'
    //   console.log('iside',m)
    // })
    this.midiv.changes.pipe().subscribe((ch=>{
      console.log('changes',ch)
      this.midiv.forEach(m=>{
        setTimeout(()=>m.data='OKKKDIVVVV')
        console.log('iside',m)
       // m.ch.detectChanges()
       // this.ch.detectChanges()

      })

    }))
    
  }
  ngAfterContentInit(): void {
    this.myContext = { $implicit: 'World', local: this.myDirec!.appMydirec };
  }

  ngOnInit() {
    this.store.data$.pipe().subscribe((d)=> console.log('data $', d))

    this.usersFiltered$ = combineLatest([this.subjectUsers.asObservable(), this.subjectFilters.asObservable()]).pipe(
      map(([users, filters]) => {
        return filters.reduce((prev, act) => prev.filter((u) => act.fnCompare(u, act.valueFilter)), users);
      })
    );
    this.store.dispatch(s=> ({...s,data:{...s.data,dataa:'dataa'}  }))
    this.store.dispatch(s=> ({...s,data:{...s.data,datab:'datab'} }))
    this.store.dispatch(s=> ({...s,data:{...s.data,datac:'datac'} }))
    this.store.dispatch(s=> ({...s,currentUser:null }))
    this.store.dispatch(s=> ({...s,currentUser:null }))
 
  }

  setUsers() {
    this.subjectUsers.next(users);
  }
  setUsers2() {
    this.subjectUsers.next(users2);
  }
  setFiltera() {
    this.setFilter({ id: 'filtera', valueFilter: 'hect',
    fnCompare:(value:User,valueFiler:string)=> value.name.includes(valueFiler) })
  
  }

  setFilterb() {
    this.setFilter({ id: 'filterb', valueFilter: 30,
    fnCompare:(value:User,valueFiler:number)=> value.age===valueFiler })
  
  }

  setFilter(filter:Filter){
    this.subjectFilters.next(updateFilter(filter
    , this.subjectFilters.value));
  }
  setMasks(){
    this.subjectMask.next(['mask1','arturo','hector']);
  }
  setMasks2(){
    this.subjectMask.next(['mask2','hect']);
  }

  getCkeck(user:User, masks:string[]){
    return masks?.some(m=> user.name.includes(m))

  }


  expandChild(user:User){
    this.subjectUsers.next(insertUpdate(this.subjectUsers.value,{...user,child:'cilldd'},(c,e)=> c.name===e.name )) 
  }


}
