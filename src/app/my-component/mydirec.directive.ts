import { Directive, Input, TemplateRef } from '@angular/core';

@Directive({
  selector: '[appMydirec]'
})
export class MydirecDirective {
  @Input('appMydirec')
  appMydirec!:string;

  constructor(public temp:TemplateRef<any>) { }

}
