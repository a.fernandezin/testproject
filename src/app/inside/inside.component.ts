import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input, OnInit, Optional } from '@angular/core';
import { AbstractGroup } from '../group/group.abstract';
import { GroupComponent } from '../group/group.component';

@Component({
  selector: 'app-inside',
  templateUrl: './inside.component.html',
  styleUrls: ['./inside.component.scss'],
 changeDetection: ChangeDetectionStrategy.OnPush, 
})
export class InsideComponent implements OnInit {

@Input()
data!:string;
@Input()
name!:string;

  constructor(@Optional()public group:AbstractGroup,public ch:ChangeDetectorRef) {
    console.log('group comp******',group)

   }

  ngOnInit(): void {
    this.data= this.group?.groupid
  }

}
