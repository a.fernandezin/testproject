import { AfterContentInit, AfterViewInit, ChangeDetectorRef, Component, ContentChild, ContentChildren, forwardRef, Input, OnInit, Optional, QueryList, SkipSelf, TemplateRef } from '@angular/core';
import { InsideComponent } from '../inside/inside.component';
import {map, tap,delay,mergeMap,defaultIfEmpty } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { query } from '@angular/animations';
import { AbstractGroup } from './group.abstract';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss'],
  providers:[{provide:AbstractGroup,useExisting:forwardRef(()=> GroupComponent )  }]
})
export class GroupComponent implements OnInit ,AfterViewInit,AfterContentInit,AbstractGroup{
   @Input()
   groupid!:string;
  @ContentChildren(InsideComponent,{descendants:true})
  listchildren!: QueryList<InsideComponent>; 
  @ContentChildren(GroupComponent,{descendants:true})
  listGroups!: QueryList<GroupComponent>; 
  @ContentChild(TemplateRef) tempref!:TemplateRef<any>;

  constructor(@Optional()@SkipSelf() public group:AbstractGroup  ,public chan:ChangeDetectorRef) { }
  ngAfterContentInit(): void {

  }



  ngAfterViewInit(): void {
  
    this.listchildren.changes.pipe(
      tap((groups)=> console.log('change children',groups,this.groupid)),
      map((listchildren: QueryList<InsideComponent>)=> listchildren.filter(child=> child.group===this)),
      tap((children)=> console.log('change children filtred',children,this.groupid)),
      ).
    subscribe()

    
    this.listGroups.changes.pipe(tap((groups)=> console.log('change groups',groups,this.groupid)),
    map((listchildren: QueryList<GroupComponent>)=> listchildren.filter(child=> child.group===this)),
     tap((children)=> console.log('change grpup children filtred',children,this.groupid)),    
    
    ).
    subscribe()


    
// setTimeout(()=>{
// console.log ( 'cukds filred', this.groupid ,this.filterChildsDirect(this.groupid))
// this.filterChildsDirect(this.groupid).forEach(c=>{
//   c.data=this.groupid;
//   c.ch.detectChanges()
// }  
// )
//    this.listGroups.changes.pipe(tap((groups)=> console.log('change groups',groups,this.groupid))).
//    subscribe()
//    this.listchildren.changes.pipe(tap((groups)=> console.log('change children',groups,this.groupid))).
//    subscribe()

// }
// //.subscribe()
// ,1)
  
  

// this.listchildren.forEach(c=> {if(!c.data)c.data=this.groupid;
//   console.log('group B**********',this.groupid) 
//   //this.chan.detectChanges()
//   //c.ch.detectChanges()
// })

}



// private filterChildsDirect(group:string)  {
//   console.log('before combine',group,this.listGroups.length)
//   console.log('length childs',group,this.listGroups.get(0)?.listchildren.length)

//  const listchildsGroups=   this.listGroups.map(group=>group.listchildren.map(child=> child)  )

//  const list:InsideComponent[] =listchildsGroups.reduce((prev,act)=> [...prev,...act],[])

//  //console.log('list childs groups' ,group, list)
//  //console.log('list childs ',group, this.listchildren.length)

//  return this.listchildren.filter(child => !(list.some(c => c === child)));
 
// }





  // private filterChilds(group:string) {
  //   console.log('before combine',group,this.listGroups.length)
  //   console.log('length childs',group,this.listGroups.get(0)?.listchildren.length)
  //   return combineLatest([this.listGroups.changes.pipe(defaultIfEmpty('groups emty')), this.listchildren.changes]).pipe(
  //     mergeMap(([listGroups, listchildren]: [QueryList<GroupComponent>, QueryList<InsideComponent>]) => {
  //       console.log('groups result',listGroups)
  //       // subcribe all groups, and get its child components
  //       return combineLatest(listGroups.map(g=> g.listchildren.changes.pipe(map((query:QueryList<InsideComponent>)=> query.map(list=> list))))).pipe(
  //       // join of all components groups
  //        map((list:InsideComponent[][])=> list.reduce((prev,act)=> [...prev,...act],[])),
  //        tap(res=>console.log('result***',res)),
  //       // filter childrencomponents with components groups 
  //        map(list=> listchildren.filter(child => !(listGroups.some(group => group.listchildren.some(c => c === child))))),            
  //         tap(res=>console.log('result filtered***',group,res))   
  //       )        

  //     }),
  //     tap(children => console.log('cihdrens group', children, this.groupid))
  //   )
  // }

  ngOnInit(): void {

  }

}
